import "./App.css";
import { ThemeProvider } from "@mui/material/styles";
import { Route, Routes } from "react-router-dom";
import theme from "./components/theme";
import SignInScreen from "./screens/sign-in/SignInScreen";
import EnterPassword from "./screens/enter-password/EnterPassword";
import { ToastNotification } from "./components/modals/ToastNotification";
import ProtectRoutes from "./components/routes/ProtectRoutes";
import MainScreen from "./screens/main/MainScreen";
import Management from "./screens/management/Management";
import { AppPathEnum } from "./enums/path-enum";

function App() {
  return (
    <>
      <ToastNotification />
      <ThemeProvider theme={theme}>
        <Routes>
          <Route path="/" element={<SignInScreen />} />
          <Route path="/sign-in" element={<SignInScreen />} />
          <Route path="/enter-password" element={<EnterPassword />} />
          <Route path="/" element={<ProtectRoutes />}>
            {/* <Route path="/" element={<MainScreen />} /> */}
            <Route path={AppPathEnum.MAIN} element={<MainScreen />} />
            <Route path={AppPathEnum.MANAGEMENT} element={<Management />} />
          </Route>
        </Routes>
      </ThemeProvider>
    </>
  );
}

export default App;
