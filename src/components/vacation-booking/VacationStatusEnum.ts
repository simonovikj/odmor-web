export enum VacationStatusEnum {
  Pending = "Pending",
  Approved = "Approved",
  Rejected = "Rejected",
}
